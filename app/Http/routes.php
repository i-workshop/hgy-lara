<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::get('about','PagesController@about')->name('about');
Route::get('contact','PagesController@contact')->name('contact');
Route::post('contact','PagesController@doContact')->name('contact');
Route::get('articles','ArticleController@index')->name('articles');
Route::get('articles/dummy','ArticleController@makedummycontent')->name('dummy');