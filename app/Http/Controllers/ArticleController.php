<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Article;
use Carbon\Carbon;

class ArticleController extends Controller
{
    public function index(){
        return view('articles.index');
    }
    
    public function makedummycontent(){
        $article = new Article;
        $article->title='teszt uj cim '.rand(1,1000);
        $article->lead='teszt';
        $article->content='bla bla';
        $article->published_at= Carbon::now();
        $article->save();
        return redirect()->back();
    }
}
