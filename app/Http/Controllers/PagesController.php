<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
class PagesController extends Controller {

    public function about() {
        $firstname = 'George';
        $lastname = 'Horváth';
        $favorite_drinks = ['Wine', 'Beer', 'Milk', 'Coffee'];
        //dd($favorite_drinks);//die és dump
        return view('about', compact('firstname', 'lastname', 'favorite_drinks'));
    }

    public function contact() {
        return view('contact');
    }
    
    public function doContact(Request $request){
        //validálás, ha 'elhasal' akkor triggereli a folyamatot és nem hajtja végre az utána lévő sort
        $this->validate($request, [
            'name'=>'required|max:50',
            'email'=>'required|email',
            'message'=>'required|min:20|max:500',
        ]);
        $data=[
            'message_text'=> $request->input('message'),
        ];
        //ha failed validation van akkor innen már nem fut a kod tovább
        
        Mail::send('emails.test', $data, function ($message) {
        $message->from('user@email.hu', 'User Name');
        $message->subject('Contact message from site');
        $message->to('info@domainnev.hu');
        
});
        
        return redirect()->back();
    }

}
