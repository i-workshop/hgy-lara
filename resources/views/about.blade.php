@extends('layouts.app')

@section('title','About')

@section('content')
<h1 class="title">About {{ $firstname }} <span style="color:brown">{{ $lastname }}</span></h1>
<h3>Kedvenc italaim</h3>
<ul>
    @forelse($favorite_drinks as $drink)
    <li>{{ $drink }}</li>
    @empty
    <li>...nincs kedvenc italom</li>
    @endforelse
</ul>

@endsection